  

# OnAir Indicator with Countdown

This project is my take on an OnAir Indicator sign. I am using it at my office to let co-workers know when I am in a zoom or teams meeting and should not be disturbed.

  

The project relies on ThingSpeak to act as a cloud broker for the OnAir status. This is done to avoid issues with the firewall at my office. Direct communication from my desk computer to a device on WiFi is not possible.

  

The status is published to ThingSpeak by a python script based on work done by Hans Scharler (http://www.nothans.com). The code has only been tested on Windows systems, so no promises as to if it will work on Linux. There is also a problem where the code will not detect camera usage on webcams that show up in windows device manager as "Imaging Devices" instead of under "Cameras".

  

## Setup

### Config File

  

1. Copy the "config.json.example" file to config.json and open it in your favorite text editor.

  

### ThingSpeak

  

1. Go to [ThingSpeak](https://thingspeak.com/) and click the “**Get Started For Free**” button to create a new account. This account is linked to a Mathworks account. So, if you already have a Mathworks account, you should log in with that account.

2. Press the “New Channel” button to create a new channel.

3. Type a name for your channel and add a description. I used "On Air Indicator".

4. Enable Field 1, and Field 2, Field 3. While the names do not matter to the script, I recommend you name Field 1, "WebCam Status", Field 2, "Message", Field 3, "Screen Display"

5. Click the **Save Channel** button to create and save your channel.

6. To send values from the Python script or Postman to ThingSpeak, you need the Write API Key. Open the “API Keys” tab and make note of it for use in the Python setup section.

7. To read the data from ThingSpeak the ESP32 will need to know the read key and channel ID. Copy the key from the ThingSpeak **Read API Keys** section and paste it into the "channelreadapikey" field in config.json, next copy the Channel ID (located under the channel title) and paste it into the "channelid" field in config.json.

### TimeZone
1. In the config.json set the "zonedata" field using the list from the [Posix Time Zone DB](https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv)
*Note: "name" and "offset" are currently unused.*
### WiFi

  

1. In "config.json" insert your WiFi SSID and Password in the corresponding fields.

  

### Build

1. Build the LittleFS file system, and upload it to the ESP32. You may need to do a full erase of the ESP32 board for the upload to complete correctly.

  

### Python

1. Install python3 on your windows system.

2. From a command prompt with administrative privileges, install the needed libraries:

* pip install opencv-python

* pip install requests

* pip install dearpygui

* pip install pathlib

3. Copy OnAir.example.ini to OnAir.ini.

4. Add your channel's Write API Key to the "*writekey*" key in the [thingspeak] section of the ini file.

5. Set the number of cameras in the [cameras] section of the ini file to reflect the number of cameras on your system.

6. Launch Windows Task Scheduler and create a task to run as frequent as you would like to check the status of your webcam. I set the task to start at 8 am on weekdays, and repeat every 5 minutes. The task should run as your own user, and only run when the user is logged on. I also made sure to check the checkbox to "Run task as soon as possible after a scheduled start is missed" on the "Settings" tab.

  

### Code

  

1. Edit "OnAir.ino" and update the define for TIMEZONE_OFFSET to match your local timezone.

2. Edit "SmartMatrix/src/esp32_i2s_parallel.c" and add the following lines (Per: https://github.com/pixelmatix/SmartMatrix/issues/171):

  

    #ifndef GPIO_PIN_MUX_REG
    #include "soc/gpio_periph.h"
    #include "esp32-hal.h"
    #include "hal/gpio_types.h"
    #endif
    
      

## Usage

  

1. Launch the python program "Message.py".

2. Enter the text you wish to display in the "Message Text" text box, and click the "Send" button.

3. The message on the display should change at the next polling interval. (30 seconds by default)

  

## Usage - Alternate

  

1. To set the message to be scrolled when Off Air you will need to send the message to ThingSpeak. You may also use something like [Postman](https://www.postman.com/). You can find a good set of [instructions](https://www.mathworks.com/help/thingspeak/writedata.html) in the MathWorks Help Center.

2. Send your text to Field 2 on your channel.

3. The text should appear on your screen after the next successful poll of the ThingSpeak service.

4. OnAir status should change when your camera is detected to be in use. The screen will update after its next successful polling of the ThingSpeak service. You can force a change by using Postman to set Field 1 to "1".

  

## Hardware

For the hardware I build a board based on the [instructable that came with HackerBoxes #065](https://www.instructables.com/HackerBox-0065-Realtime/). I left out the RTC in my build. In theory any board supported by the SmartMatrix library should work, you may have to adjust the code accordingly though.

  

![schematic for HUB75Blaster board](https://content.instructables.com/ORIG/F4Q/2J41/KND6FADJ/F4Q2J41KND6FADJ.png?auto=webp&frame=1&width=931&fit=bounds&md=87fe2f53846edcec43ba287e45aad9c6)

  

## ToDo

  

- TODO: Add WiFi Manager support to make configuration easier

  

## Attribution

  

- Hans Scharler (http://www.nothans.com)

- Hourglass Image: https://icons-for-free.com/hourglass-131982518802441865/

- Santa Image: https://www.shutterstock.com/image-vector/8-bit-pixel-santa-claus-bag-1586339716

  

> Written with [StackEdit](https://stackedit.io/).