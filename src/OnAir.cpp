#include <Arduino.h>
#include <LittleFS.h>
#include "OnAir.h"

// Panel for my desk at work has G and B channels reversed and requires a custom pin definition
#define WORK true

#if WORK
#include "MatrixHardware_ESP32_V0_RBG_reversed.h" // Customized include file to reverse Green and Blue channel pin definitions.
#else
#include <MatrixHardware_ESP32_V0.h>
#endif

#include <ArduinoJson.h>
#include <SmartMatrix.h>
#include <pngle.h>
#include <NTPClient.h>
#include "TimeLib.h"
#include <cmath>

#include <WiFi.h>
#include <WiFiUdp.h>

#include "ThingSpeak.h" // always include thingspeak header file after other header files and custom macros
#include "colors.h"

/**********************
 *  Editable Settings
 **********************/

#define DEBUG
#define MULTIRESETDETECTOR_DEBUG true // false

/*****************************
 * Nothing to edit past here
 *****************************/

// These defines must be put before #include <ESP_MultiResetDetector.h>
// to select where to store MultiResetDetector's variable.
// For ESP32, You must select one to be true (EEPROM or SPIFFS)
#define ESP_MRD_USE_LITTLEFS true
#define ESP_MRD_USE_SPIFFS false
#define ESP_MRD_USE_EEPROM false

// Number of subsequent resets during MRD_TIMEOUT to activate
#define MRD_TIMES 5

// Number of seconds after reset during which a
// subsequent reset will be considered a multi reset.
#define MRD_TIMEOUT 10

// RTC/EEPROM Memory Address for the MultiResetDetector to use
#define MRD_ADDRESS 0

#ifndef LED_BUILTIN
#define LED_BUILTIN 2 // Pin D2 mapped to pin GPIO2/ADC12 of ESP32, control on-board LED
#endif

#define LED_OFF LOW
#define LED_ON HIGH

#include <ESP_MultiResetDetector.h> //https://github.com/khoih-prog/ESP_MultiResetDetector

char ssid[32];
char pass[64];

unsigned long previousMillis = 0;
const long pollInterval = 30000;

#define USE_ADAFRUIT_GFX_LAYERS
#define COLOR_DEPTH 24                                         // Choose the color depth used for storing pixels in the layers: 24 or 48 (24 is good for most sketches - If the sketch uses type `rgb24` directly, COLOR_DEPTH must be 24)
const uint16_t kMatrixWidth = 64;                              // Set to the width of your display, must be a multiple of 8
const uint16_t kMatrixHeight = 32;                             // Set to the height of your display
const uint8_t kRefreshDepth = 36;                              // Tradeoff of color quality vs refresh rate, max brightness, and RAM usage.  36 is typically good, drop down to 24 if you need to.  On Teensy, multiples of 3, up to 48: 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48.  On ESP32: 24, 36, 48
const uint8_t kDmaBufferRows = 4;                              // known working: 2-4, use 2 to save RAM, more to keep from dropping frames and automatically lowering refresh rate.  (This isn't used on ESP32, leave as default)
const uint8_t kPanelType = SM_PANELTYPE_HUB75_32ROW_MOD16SCAN; // Choose the configuration that matches your panels.  See more details in MatrixCommonHub75.h and the docs: https://github.com/pixelmatix/SmartMatrix/wiki
const uint32_t kMatrixOptions = (SM_HUB75_OPTIONS_NONE);       // see docs for options: https://github.com/pixelmatix/SmartMatrix/wiki
const uint8_t kIndexedLayerOptions = (SM_INDEXED_OPTIONS_NONE);
const uint8_t kBackgroundLayerOptions = (SM_BACKGROUND_OPTIONS_NONE);
const uint8_t kScrollingLayerOptions = (SM_SCROLLING_OPTIONS_NONE);

const int defaultBrightness = (35 * 255) / 100; // dim: 35% brightness

SMARTMATRIX_ALLOCATE_BUFFERS(matrix, kMatrixWidth, kMatrixHeight, kRefreshDepth, kDmaBufferRows, kPanelType, kMatrixOptions);
SMARTMATRIX_ALLOCATE_BACKGROUND_LAYER(backgroundLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kBackgroundLayerOptions);
SMARTMATRIX_ALLOCATE_SCROLLING_LAYER(scrollingLayer1, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kScrollingLayerOptions);

WiFiClient client;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
MultiResetDetector *mrd;
pngle_t *pngle = nullptr;
Config config;

long minutesRemain = 0;
int daysRemain = 0;

// Used to see if ThinkSpeak has been updated since last polling... no need to change the message if not
String holdUpdateStamp = "2024-11-06T20:00:00Z"; // Set a resonable default

// Keep track of e last day we updated the display
int lastCheckedDay = -1;

// Default color for the scrolling text
rgb24 scrollColor = WHITE;

/**
 * Read the config values
 */
void readConfig()
{
    if (!LittleFS.begin())
    {
        Serial.println("Failed to mount LittleFS");
        return;
    }

    File configFile = LittleFS.open("/config.json", "r");
    if (!configFile)
    {
        Serial.println("Failed to open config file");
        return;
    }

    JsonDocument doc;
    DeserializationError error = deserializeJson(doc, configFile);
    if (error)
    {
        Serial.println("Failed to parse config file");
        Serial.println(error.c_str());
        return;
    }

    // Extract WiFi settings
    strlcpy(config.ssid, doc["wifi"]["ssid"] | "", sizeof(config.ssid));
    strlcpy(config.password, doc["wifi"]["password"] | "", sizeof(config.password));

    // Extract timezone settings
    strlcpy(config.timezone, doc["timezone"]["name"] | "", sizeof(config.timezone));
    config.timezoneOffset = doc["timezone"]["offset"] | 0;
    strlcpy(config.timezoneData, doc["timezone"]["zonedata"] | "", sizeof(config.timezoneData));

    // Extract ThingSpeak settings
    config.channelId = doc["thingspeak"]["channelid"] | 0;
    strlcpy(config.channelReadApiKey, doc["thingspeak"]["channelreadapikey"] | "", sizeof(config.channelReadApiKey));

    configFile.close();
}

/**
 * Save config values to LittleFS
 */
void writeConfig(const Config &config)
{
    File configFile = LittleFS.open("/config.json", "w");
    if (!configFile)
    {
        Serial.println("Failed to open config file for writing");
        return;
    }

    JsonDocument doc;
    doc["wifi"]["ssid"] = config.ssid;
    doc["wifi"]["password"] = config.password;
    doc["timezone"]["name"] = config.timezone;
    doc["timezone"]["offset"] = config.timezoneOffset;
    doc["timezone"]["zonedata"] = config.timezoneData;
    doc["thingspeak"]["channelid"] = config.channelId;
    doc["thingspeak"]["channelreadapikey"] = config.channelReadApiKey;

    if (serializeJson(doc, configFile) == 0)
    {
        Serial.println("Failed to write to config file");
    }
    configFile.close();
}

/**
 * Draw a bitmap on screen
 */
void drawBitmap(int16_t x, int16_t y, const gimp32x32bitmap *bitmap)
{

    for (unsigned int i = 0; i < bitmap->height; i++)
    {
        for (unsigned int j = 0; j < bitmap->width; j++)
        {
            SM_RGB pixel = {bitmap->pixel_data[(i * bitmap->width + j) * 3 + 0],
                            bitmap->pixel_data[(i * bitmap->width + j) * 3 + 1],
                            bitmap->pixel_data[(i * bitmap->width + j) * 3 + 2]};
            if (COLOR_DEPTH == 48)
            {
                pixel.red = pixel.red << 8;
                pixel.green = pixel.green << 8;
                pixel.blue = pixel.blue << 8;
            }

            backgroundLayer.drawPixel(x + j, y + i, pixel);
        }
    }
}

/**
 * Function to sync NTP time with the system time
 */
void syncTime()
{
    timeClient.update();
    time_t rawTime = timeClient.getEpochTime();
    struct timeval tv = {rawTime, 0};
    settimeofday(&tv, nullptr); // Set system time

    // Print current NTP time for debugging
    Serial.print("Current NTP Time: ");
    Serial.println(rawTime);
    Serial.print("Formatted Time: ");
    Serial.println(ctime(&rawTime)); // Display the formatted time
}

/**
 * Compute the days until a given day / month
 */
int calculateDaysUntil(int targetMonth, int targetDay)
{
    time_t now = time(nullptr);
    struct tm *nowTm = localtime(&now);

    // Set up the target date for this year.
    struct tm targetDate = *nowTm;
    targetDate.tm_mon = targetMonth - 1; // tm_mon is 0-based
    targetDate.tm_mday = targetDay;
    targetDate.tm_hour = 0;
    targetDate.tm_min = 0;
    targetDate.tm_sec = 0;

    // Convert target date to time_t format.
    time_t targetTime = mktime(&targetDate);

    // If the target date has already passed this year, set it to the same date next year.
    if (targetTime < now)
    {
        targetDate.tm_year += 1; // Move to next year
        targetTime = mktime(&targetDate);
    }

    // Calculate the difference in seconds, then convert to days with rounding up.
    double secondsUntilTarget = difftime(targetTime, now);
    int daysUntilTarget = ceil(secondsUntilTarget / (60 * 60 * 24));

    return daysUntilTarget;
}

/**
 * Compute the days until Halloween
 */
int daysUntilHalloween()
{
    int myDaysRemain = calculateDaysUntil(10, 31); // Halloween: October 31

#ifdef DEBUG
    if (myDaysRemain == 0)
    {
        Serial.println("It's Halloween!");
    }
    else
    {
        Serial.print("Days until Halloween: ");
        Serial.println(myDaysRemain);
    }
#endif
    return myDaysRemain;
}

/**
 * Compute the days until Christmas
 */
int daysUntilChristmas()
{
    int myDaysRemain = calculateDaysUntil(12, 25); // Christmas: December 25

#ifdef DEBUG
    if (myDaysRemain == 0)
    {
        Serial.println("It's Christmas!");
    }
    else
    {
        Serial.print("Days until Christmas: ");
        Serial.println(myDaysRemain);
    }
#endif
    return myDaysRemain;
}

/**
 * Setup Function
 */
void setup()
{
    Serial.begin(115200); // Initialize serial
    pinMode(LED_BUILTIN, OUTPUT);

    readConfig();

    // Print the config to verify
    Serial.println("WiFi SSID: " + String(config.ssid));
    Serial.println("WiFi Password: " + String(config.password));
    Serial.println("Timezone: " + String(config.timezone));
    Serial.println("Timezone Offset: " + String(config.timezoneOffset));
    Serial.println("Timezone Data: " + String(config.timezoneData));
    Serial.println("ThingSpeak Channel ID: " + String(config.channelId));
    Serial.println("ThingSpeak Read API Key: " + String(config.channelReadApiKey));

    // setup matrix
    matrix.addLayer(&backgroundLayer);
    matrix.addLayer(&scrollingLayer1);
    matrix.begin();
    matrix.setBrightness(defaultBrightness);

    drawBootScreen();

    Serial.print(F("\nStarting ESP_MultiResetDetector minimal on "));
    Serial.print(ARDUINO_BOARD);

#if ESP_MRD_USE_LITTLEFS
    Serial.println(F(" using LittleFS"));
#elif ESP_MRD_USE_SPIFFS
    Serial.println(F(" using SPIFFS"));
#else
    Serial.println(F(" using EEPROM"));
#endif

    Serial.println(ESP_MULTI_RESET_DETECTOR_VERSION);

    mrd = new MultiResetDetector(MRD_TIMEOUT, MRD_ADDRESS);

    if (mrd->detectMultiReset())
    {
        // TODO: Add code to trigger config portal here
        Serial.println("Multi Reset Detected");
        digitalWrite(LED_BUILTIN, LED_ON);
    }
    else
    {
        // TODO: Load settings from SPIFFS
        Serial.println("No Multi Reset Detected");
        digitalWrite(LED_BUILTIN, LED_OFF);
    }

    scrollingLayer1.setMode(wrapForward);
    scrollingLayer1.setColor({0xff, 0xff, 0xff});
    scrollingLayer1.setSpeed(10);
    scrollingLayer1.setFont(gohufont11b);
    scrollingLayer1.setOffsetFromTop((kMatrixHeight / 2) - 5);

    Serial.println("Begin WiFi");

    WiFi.mode(WIFI_STA);
    ThingSpeak.begin(client); // Initialize ThingSpeak
    Serial.println("Begin NTP");

    timeClient.begin();
    setTimezone(config.timezoneData);
    syncTime(); // Sync NTP time with system time
}

/**
 * This function sets the timezone
 */
void setTimezone(String timezone)
{
    Serial.printf("  Setting Timezone to %s\n", timezone.c_str());
    setenv("TZ", timezone.c_str(), 1); //  Now adjust the TZ.  Clock settings are adjusted to show the new local time
    tzset();
}

/**
 * How many digits are in a given number
 */
int countDigit(long long n)
{
    int count = 0;
    while (n != 0)
    {
        n = n / 10;
        ++count;
    }
    return count;
}

/**
 * This function calculates the date of Thanksgiving for a given year.
 */
tm calculateThanksgiving(int year)
{
    // Set the date to November 1 of the specified year
    tm thanksgiving = {};
    thanksgiving.tm_year = year - 1900; // tm_year is years since 1900
    thanksgiving.tm_mon = 10;           // November (0-based index, so 10 = November)
    thanksgiving.tm_mday = 1;
    thanksgiving.tm_hour = 0;
    thanksgiving.tm_min = 0;
    thanksgiving.tm_sec = 0;

    // Convert to time_t to use localtime and adjust for the weekday
    time_t firstOfNovember = mktime(&thanksgiving);
    tm *firstOfNovemberTm = localtime(&firstOfNovember);

    // Find the first Thursday in November
    int daysToFirstThursday = (4 - firstOfNovemberTm->tm_wday + 7) % 7;

    // Thanksgiving is the fourth Thursday, which is 3 weeks after the first Thursday
    thanksgiving.tm_mday = 1 + daysToFirstThursday + (3 * 7);

    return thanksgiving;
}

/**
 * Returns the current year, assuming the internal clock is correct.
 */
int getCurrentYear()
{
    time_t now = time(nullptr);         // Get the current time
    struct tm *nowTm = localtime(&now); // Convert to tm structure for local timezone
    return nowTm->tm_year + 1900;       // tm_year is years since 1900
}

/**
 * Returns the current day of year, assuming the internal clock is correct.
 */
int getCurrentYearDay()
{
    time_t now = time(nullptr);         // Get the current time
    struct tm *nowTm = localtime(&now); // Convert to tm structure for local timezone
    return nowTm->tm_yday;              // day of the year
}

/**
 * Return the number of days until the next Thanksgiving. 
 */
int daysUntilThanksgiving()
{

    tm thanksgiving = calculateThanksgiving(getCurrentYear());
    int myDaysRemain = calculateDaysUntil(thanksgiving.tm_mon + 1, thanksgiving.tm_mday); // Thanksgiving

#ifdef DEBUG
    if (myDaysRemain == 0)
    {
        Serial.println("It's Thanksgiving!");
    }
    else
    {
        Serial.print("Days until Thanksgiving: ");
        Serial.println(myDaysRemain);
    }
#endif
    return myDaysRemain;
}

/**
 * ON Air 
 */
void onAirScreen()
{
    scrollingLayer1.stop();
    backgroundLayer.fillScreen(ONAIR_RED);
    backgroundLayer.swapBuffers();

    scrollingLayer1.setMode(wrapForward);
    scrollingLayer1.setColor(WHITE);
    scrollingLayer1.setSpeed(10);
    scrollingLayer1.setFont(gohufont11b);
    scrollingLayer1.setOffsetFromTop((kMatrixHeight / 2) - 5);
    scrollingLayer1.start("ON AIR", -1);
}

/**
 * Main Loop
 */
void loop()
{

    unsigned long currentMillis = millis();
    int statusCode = 0;

    mrd->loop();

    // Connect or reconnect to WiFi
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.print("Attempting to connect to SSID: ");
        Serial.println(config.ssid);
        while (WiFi.status() != WL_CONNECTED)
        {
            WiFi.begin(config.ssid, config.password); // Connect to WPA/WPA2 network. Change this line if using open or WEP network
            Serial.print(".");
            delay(5000);
        }
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
    }
    // Poll NTP server
    timeClient.update();


    // don't show a screen until NTP returns a date past: Tuesday, November 5, 2024 9:46:40 AM
    if (currentMillis - previousMillis >= pollInterval && timeClient.getEpochTime() > 1730800000)
    {
        // Sync the system time with NTP
        syncTime();

        // What day is it?
        int currentDay = getCurrentYearDay();

        // Save previous polling
        previousMillis = currentMillis;

        int onAirStatus = ThingSpeak.readIntField(config.channelId, 1, config.channelReadApiKey);
        String netMessage = ThingSpeak.readStringField(config.channelId, 2, config.channelReadApiKey);
        int screenDisplay = ThingSpeak.readIntField(config.channelId, 3, config.channelReadApiKey);
        String myUpdateStamp = ThingSpeak.readCreatedAt (config.channelId, config.channelReadApiKey);
        

        // Check the status of the read operation to see if it was successful
        statusCode = ThingSpeak.getLastReadStatus();

        // we had a successful read
        if (statusCode == TS_OK_SUCCESS) 
        {
            // Either the time stamp from ThingSpeak has changed, or the day has changed, update the screen
            if ( myUpdateStamp != holdUpdateStamp || currentDay != lastCheckedDay )
            {

                Serial.println("On Air Status: " + String(onAirStatus));
                Serial.println("Message: " + netMessage);
                Serial.println("Screen Display: " + String(screenDisplay));
                Serial.println("Updated: " + myUpdateStamp);

                // Update the lastCheckedDay to the current day after the check
                lastCheckedDay = currentDay;

                // Store the possible new update stamp for future checks
                holdUpdateStamp = myUpdateStamp;

                if (onAirStatus == 1)
                {
                    onAirScreen();
                    Serial.println(F("ON AIR"));
                }
                else
                {
                    scrollingLayer1.stop();

                    if (screenDisplay == 1)
                    {
                        scrollColor = WHITE;
                        christmas_countdown(daysUntilChristmas());
                    }
                    else if (screenDisplay == 2)
                    {
                        scrollColor = WHITE;
                        halloween_countdown(daysUntilHalloween());
                    }
                    else if (screenDisplay == 3)
                    {
                        scrollColor = WHITE;
                        drawTacoScreen();
                    }
                    else if (screenDisplay == 4)
                    {
                        scrollColor = WHITE;
                        drawWccScreen();
                    }
                    else if (screenDisplay == 5)
                    {
                        scrollColor = WHITE;
                        drawFishScreen();
                    }
                    else if (screenDisplay == 6)
                    {
                        scrollColor = WHITE;
                        drawSushiScreen();
                    }
                    else if (screenDisplay == 7)
                    {
                        scrollColor = WHITE;
                        drawPalmScreen();
                    }
                    else if (screenDisplay == 8)
                    {
                        scrollColor = WHITE;
                        thanksgiving_countdown(daysUntilThanksgiving());
                    }
                    else if (screenDisplay == 9)
                    {
                        scrollColor = MARIO_YELLOW;
                        drawMarioScreen();
                    }
                    else
                    {
                        // Default display...
                        scrollColor = WHITE;
                        drawWccScreen();
                    }

                    scrollingLayer1.setMode(wrapForward);
                    scrollingLayer1.setColor(scrollColor);
                    scrollingLayer1.setSpeed(20);
                    scrollingLayer1.setFont(font5x7);
                    scrollingLayer1.setOffsetFromTop(0);

                    /* Read message from Thingspeak */
                    int n = netMessage.length();
#ifdef DEBUG
                    Serial.println("Message Length: " + String(n));
#endif
                    if (n > 0)
                    {
                        char msgBuffer[n + 1]; // make sure to leave room for the string terminator
                        strcpy(msgBuffer, netMessage.c_str());
                        scrollingLayer1.start(msgBuffer, -1);
                    }

                    Serial.println(F("OFF AIR"));
                }
            }
            else
            {
                Serial.println("No work to do.");    
            }
        }
        else
        {
            Serial.println("Problem reading channel. HTTP error code " + String(statusCode));
        }
    }
}

/**
 * Draw the "I want sushi" screen.
 */
void drawSushiScreen()
{
    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.drawRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, SUSHI_BLUE);

    x = 5;
    y = (kMatrixHeight - sushi.height);
    drawBitmap(x, y, (const gimp32x32bitmap *)&sushi);

    backgroundLayer.setFont(font3x5);
    backgroundLayer.drawString(36, 10, WCC_GOLD, "John   ");
    backgroundLayer.drawString(36, 17, WCC_GOLD, "Wohlers");
    backgroundLayer.swapBuffers();
}

/**
 * Taco Screen
 */
void drawTacoScreen()
{
    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.drawRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, WHITE);

    // Position taco bitmap 5 pixels from the left, and at the bottom of the display
    x = 5;
    y = (kMatrixHeight - taco.height) + 5;
    // to use drawBitmap, must cast the pointer to taco as (const gimp32x32bitmap*)
    drawBitmap(x, y, (const gimp32x32bitmap *)&taco);

    backgroundLayer.setFont(gohufont11b);

    int leftEdgeOffset = taco.width + 5;
    int textBoxWidth = kMatrixWidth - leftEdgeOffset;

    // Reusing the code for days, casue i'm lazy...
    int numChars = 4;
    int textPosition = (textBoxWidth / 2) - ((numChars * 6) / 2);
    backgroundLayer.drawString(leftEdgeOffset + textPosition, 8, GREEN, "Taco");
    backgroundLayer.drawString(leftEdgeOffset + textPosition, 17, GREEN, "Tues");
    backgroundLayer.swapBuffers();
}

/**
 * Fish Screen
 */
void drawFishScreen()
{
    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.drawRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, WHITE);

    // Position bitmap 5 pixels from the left, and at the bottom of the display
    x = 5;
    y = (kMatrixHeight - fish.height);
    // to use drawBitmap, must cast the pointer to (const gimp32x32bitmap*)
    drawBitmap(x, y, (const gimp32x32bitmap *)&fish);

    backgroundLayer.setFont(gohufont11b);

    int leftEdgeOffset = fish.width + 5;
    int textBoxWidth = kMatrixWidth - leftEdgeOffset;

    // Reusing the code for days, casue i'm lazy...
    int numChars = 5;
    int textPosition = (textBoxWidth / 2) - ((numChars * 6) / 2);
    backgroundLayer.drawString(leftEdgeOffset + textPosition, 8, GREEN, "Hello");
    backgroundLayer.drawString(leftEdgeOffset + textPosition, 17, GREEN, "There");
    backgroundLayer.swapBuffers();
}

/**
 * WCC Name Plate
 */
void drawWccScreen()
{
    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.drawRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, WHITE);

    // Position bitmap 5 pixels from the left, and at the bottom of the display
    x = 5;
    y = (kMatrixHeight - wcc_shield.height);
    // to use drawBitmap, must cast the pointer to (const gimp32x32bitmap*)
    drawBitmap(x, y, (const gimp32x32bitmap *)&wcc_shield);

    backgroundLayer.setFont(font3x5);

    backgroundLayer.drawString(36, 10, WCC_GOLD, "John   ");
    backgroundLayer.drawString(36, 17, WCC_GOLD, "Wohlers");
    backgroundLayer.swapBuffers();
}

void drawMarioScreen() 
{
    backgroundLayer.fillScreen(MARIO_BLUE);
    //TODO: Make this more SMB specific ground
    backgroundLayer.drawFastHLine(0, kMatrixWidth, kMatrixHeight - 3, MARIO_GRND);
    backgroundLayer.drawFastHLine(0, kMatrixWidth, kMatrixHeight - 2, MARIO_GRND);
    backgroundLayer.drawFastHLine(0, kMatrixWidth, kMatrixHeight - 1, MARIO_GRND);
    
    // Position bitmap 5 pixels from the left 5 from top
    drawPngFromLittleFS("/mario.png", 5, 5);

//    backgroundLayer.setFont(font3x5);

//    backgroundLayer.drawString(36, 10, {0xff, 0xea, 0x63}, "John   ");
//    backgroundLayer.drawString(36, 17, {0xff, 0xea, 0x63}, "Wohlers");
    backgroundLayer.swapBuffers();

}


/**
 *  Palm Tree screen
 */
void drawPalmScreen()
{
    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.fillRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, BLUE);

    // Position bitmap 5 pixels from the left, and at the bottom of the display
    x = 5;
    y = (kMatrixHeight - palmtree.height);
    // to use drawBitmap, must cast the pointer to (const gimp32x32bitmap*)
    drawBitmap(x, y, (const gimp32x32bitmap *)&palmtree);

    backgroundLayer.setFont(font3x5);

    backgroundLayer.drawString(36, 10, WCC_GOLD, "Gone   ");
    backgroundLayer.drawString(36, 17, WCC_GOLD, "Fishin'");
    backgroundLayer.swapBuffers();
}

/**
 * Hourglass
 */
void drawBootScreen()
{
    // Draw a boot screen so we know the matrix has initialized, and we are jsut awaiting WiFi and data.
#ifdef DEBUG
    Serial.println("Draw Boot Screen");
#endif
    int x, y;
    x = (kMatrixWidth / 2 - hourglass.width / 2);
    y = (kMatrixHeight / 2 - hourglass.height / 2) + 4;
    // to use drawBitmap, must cast the pointer to hourglass as (const gimp32x32bitmap*)
    drawBitmap(x, y, (const gimp32x32bitmap *)&hourglass);

    backgroundLayer.setFont(font5x7);
    backgroundLayer.drawString(7, 4, WHITE, "Loading...");
    backgroundLayer.swapBuffers();
}

/**
 * Christmas Countdown Screen
 */
void christmas_countdown(int myDaysRemaining)
{

    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.drawRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, WHITE);

    // Center Santa in the middle of the screen if we wanted to...
    // x = (kMatrixWidth / 2) - (santa.width/2);
    // y = (kMatrixHeight / 2) - (santa.height/2);

    // Position santa bitmap 5 pixels from the left, and at the bottom of the display
    x = 5;
    y = (kMatrixHeight - santa.height);
    // to use drawBitmap, must cast the pointer to santa as (const gimp32x32bitmap*)
    drawBitmap(x, y, (const gimp32x32bitmap *)&santa);

    backgroundLayer.setFont(gohufont11b);

    // Calculate Countdown Centering
    int leftEdgeOffset = santa.width + 5;
    int textBoxWidth = kMatrixWidth - leftEdgeOffset;
    int numDigits = countDigit(myDaysRemaining);
    int daysPosition = (textBoxWidth / 2) - ((numDigits * 6) / 2);

    // Convert the integer to a character array for drawString
    char buffer[sizeof(numDigits) * 8 + 1];
    itoa(myDaysRemaining, buffer, 10);
    backgroundLayer.drawString(leftEdgeOffset + daysPosition, 8, GREEN, buffer);

    // Deal with plural -vs- singular text
    int numChars = 4;
    char remainingString[5] = "Days";
    if (myDaysRemaining == 1)
    {
        numChars = 3;
        strcpy(remainingString, "Day");
    }

    int remainingPosition = (textBoxWidth / 2) - ((numChars * 6) / 2);
    backgroundLayer.drawString(leftEdgeOffset + remainingPosition, 17, GREEN, remainingString);
    backgroundLayer.swapBuffers();
}

/**
 * Halloween Countdown Screen
 */
void halloween_countdown(int myDaysRemaining)
{

    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.drawRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, WHITE);

    // Position jack-o-lantern bitmap 5 pixels from the left, and at the bottom of the display
    x = 5;
    y = (kMatrixHeight - jack.height);
    // to use drawBitmap, must cast the pointer to jack-o-lantern as (const gimp32x32bitmap*)
    drawBitmap(x, y, (const gimp32x32bitmap *)&jack);

    backgroundLayer.setFont(gohufont11b);

    // Calculate Countdown Centering
    int leftEdgeOffset = jack.width + 5;
    int textBoxWidth = kMatrixWidth - leftEdgeOffset;
    int numDigits = countDigit(myDaysRemaining);
    int daysPosition = (textBoxWidth / 2) - ((numDigits * 6) / 2);

    // Convert the integer to a character array for drawString
    char buffer[sizeof(numDigits) * 8 + 1];
    itoa(myDaysRemaining, buffer, 10);
    backgroundLayer.drawString(leftEdgeOffset + daysPosition, 8, PALE_GREEN, buffer);

    // Deal with plural -vs- singular text
    int numChars = 4;
    char remainingString[5] = "Days";
    if (myDaysRemaining == 1)
    {
        numChars = 3;
        strcpy(remainingString, "Day");
    }

    int remainingPosition = (textBoxWidth / 2) - ((numChars * 6) / 2);
    backgroundLayer.drawString(leftEdgeOffset + remainingPosition, 17, GREEN, remainingString);
    backgroundLayer.swapBuffers();
}

/**
 * Thanksgiving Countdown Screen
 */
void thanksgiving_countdown(int myDaysRemaining)
{

    int x, y;
    backgroundLayer.fillScreen({0, 0, 0});
    backgroundLayer.drawRectangle(0, kMatrixHeight - 3, kMatrixWidth, kMatrixHeight, WHITE);

    // Position bitmap 0 pixels from the left 6 from top
    drawPngFromLittleFS("/turkey_26_26.png", 0, 6);

    backgroundLayer.setFont(gohufont11b);

    // Calculate Countdown Centering
    int leftEdgeOffset = 26 + x;
    int textBoxWidth = kMatrixWidth - leftEdgeOffset;
    int numDigits = countDigit(myDaysRemaining);
    int daysPosition = (textBoxWidth / 2) - ((numDigits * 6) / 2);

    // Convert the integer to a character array for drawString
    char buffer[sizeof(numDigits) * 8 + 1];
    itoa(myDaysRemaining, buffer, 10);
    backgroundLayer.drawString(leftEdgeOffset + daysPosition, 8, PALE_GREEN, buffer);

    // Deal with plural -vs- singular text
    int numChars = 4;
    char remainingString[5] = "Days";
    if (myDaysRemaining == 1)
    {
        numChars = 3;
        strcpy(remainingString, "Day");
    }

    int remainingPosition = (textBoxWidth / 2) - ((numChars * 6) / 2);
    backgroundLayer.drawString(leftEdgeOffset + remainingPosition, 17, GREEN, remainingString);
    backgroundLayer.swapBuffers();
}

// Callback function for PNG pixel rendering
// Updated callback function with offset handling
void pngDrawCallback(pngle_t *pngle, uint32_t x, uint32_t y, uint32_t w, uint32_t h, unsigned char *rgba) {
    // Retrieve offsets
    OffsetData* offsets = (OffsetData*)pngle_get_user_data(pngle);
    int x_offset = offsets->x_offset;
    int y_offset = offsets->y_offset;

    // Apply offsets and ensure coordinates are within display boundaries
    int x_adj = x + x_offset;
    int y_adj = y + y_offset;

    if (x_adj < kMatrixWidth && y_adj < kMatrixHeight) {
        uint8_t r = rgba[0];
        uint8_t g = rgba[1];
        uint8_t b = rgba[2];
        uint8_t a = rgba[3];
        
        // Only draw non-transparent pixels
        if (a > 0) {
            backgroundLayer.drawPixel(x_adj, y_adj, {r, g, b});
        }
    }
}


// Function to load and render PNG from LittleFS at specified coordinates
void drawPngFromLittleFS(const char* filePath, int x_offset, int y_offset) {
    // Initialize LittleFS if not already done
    if (!LittleFS.begin()) {
        Serial.println("Failed to mount LittleFS");
        return;
    }

    // Open the PNG file
    File file = LittleFS.open(filePath, "r");
    if (!file) {
        Serial.println("Failed to open PNG file");
        return;
    }

    // Initialize pngle instance
    pngle = pngle_new();
    pngle_set_draw_callback(pngle, pngDrawCallback);

    // Set offsets as user data for the callback
    OffsetData offsets = { x_offset, y_offset };
    pngle_set_user_data(pngle, &offsets);

    // Read file in chunks and feed to pngle
    const size_t bufferSize = 1024;
    uint8_t buffer[bufferSize];
    while (file.available()) {
        size_t bytesRead = file.read(buffer, bufferSize);
        if (pngle_feed(pngle, buffer, bytesRead) < 0) {
            Serial.printf("PNG decoding error: %s\n", pngle_error(pngle));
            break;
        }
    }

    // Clean up
    pngle_destroy(pngle);
    file.close();
}