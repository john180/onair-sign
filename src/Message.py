"""

This Python script allows for setting messages on the LED display.


Dependencies

    * pip install dearpygui
    * pip install requests
    * pip install pathlib
    
Setup
    
    * Copy OnAir.example.ini to OnAir.ini. Add your channel's Write API Key to the WriteKey key in the thingspeak section.

Modified: Dec 1, 2021 by John Wohlers

"""
import dearpygui.dearpygui as dpg
import requests
import pathlib
import sys
from configparser import ConfigParser


path = pathlib.Path(__file__).parent.resolve()
config = ConfigParser()
dataset = config.read(str(path) + '/OnAir.ini')

if len(dataset) != 1:
    raise ValueError("Failed to open config file.")

thingspeakWriteURL = 'https://api.thingspeak.com/update.json'

try:
    thingSpeakWriteKey = config.get('thingspeak', 'writekey')
except:
    print('Could not read configuration file')
    sys.exit(1)  



def send_callback():
    print(dpg.get_value(1))
    postMessageToThingSpeak(dpg.get_value(1))
    return

def lunch_callback():
    print("Lunch Time")
    postMessageToThingSpeak("John Is Out To Lunch")
    return

def break_callback():
    print("Break")
    postMessageToThingSpeak("Be back in a few minutes")
    return

    
def postMessageToThingSpeak(valueToPost):

    thingSpeakWriteData = {'field2': valueToPost, 'api_key': thingSpeakWriteKey}

    thingspeakRequest = requests.post(thingspeakWriteURL, data = thingSpeakWriteData)

    if thingspeakRequest.ok:
        dpg.set_value(2, "Message Updated")
    else:
        dpg.set_value(2, "Message Update Failed")

    return thingspeakRequest
    

def main():
    dpg.create_context()
    dpg.create_viewport(title='Message Board',width=400, height=200)
    dpg.setup_dearpygui()
    
    dpg.set_viewport_small_icon(str(path) + "/images/icons8-led-light-16.ico")
    dpg.set_viewport_large_icon(str(path) + "/images/icons8-led-light-24.ico")
    
    with dpg.window(label="",width=400, height=200, tag="Primary Window"):
        with dpg.group(horizontal=True) as group1:
            dpg.add_button(label="Lunch", callback=lunch_callback, width=60, height=40)
            dpg.add_button(label="Break", callback=break_callback, width=60, height=40)
        with dpg.group(horizontal=True) as group2:
            dpg.add_text("Message Text")
            dpg.add_input_text(tag=1, label="", default_value="Ho! Ho! Ho!           Happy Holidays!")
        dpg.add_button(label="Send", callback=send_callback, width=365, height=40)
        with dpg.group(horizontal=True,pos=(10,180)) as group3:
            dpg.add_text("Status:")            
            dpg.add_text("", tag=2)


    dpg.show_viewport()
    dpg.set_primary_window("Primary Window", True)
    dpg.start_dearpygui()
    dpg.destroy_context()
 

if __name__ == '__main__':
    main()