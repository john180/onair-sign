// OnAir.h
#ifndef ONAIR_H
#define ONAIR_H

#include <pngle.h>
#include "gimpbitmap.h"
#include "pixel-santa.c"
#include "pixel-jack-o-lantern.c"
#include "pixel-turkey.c"
#include "pixel-taco.c"
#include "pixel-hourglass.c"
#include "pixel-wcc-shield.c"
#include "pixel-fish.c"
#include "pixel-sushi.c"
#include "pixel-palmtree.c"

struct Config {
    char ssid[32];
    char password[64];
    char timezone[32];
    char timezoneData[64];
    int timezoneOffset;
    unsigned long channelId;
    char channelReadApiKey[32];
};

// Struct to store offset data
struct OffsetData {
    int x_offset;
    int y_offset;
};


void setTimezone(String timezone);
void setup();
void syncTime();
void readConfig();
void writeConfig(const Config &config);

void drawBitmap(int16_t x, int16_t y, const gimp32x32bitmap* bitmap);
void pngDrawCallback(pngle_t *pngle, uint32_t x, uint32_t y, uint32_t w, uint32_t h, unsigned char *rgba);
void drawPngFromLittleFS(const char* filePath, int x_offset, int y_offset);

int countDigit(long long n);

int calculateDaysUntil(int targetMonth, int targetDay);
int getCurrentYear();
int getCurrentYearDay();

tm calculateThanksgiving(int year);
int daysUntilHalloween();
int daysUntilChristmas();
int daysUntilThanksgiving();

void drawBootScreen();
void drawPalmScreen();
void drawWccScreen();
void drawSushiScreen();
void drawFishScreen();
void drawTacoScreen();
void drawMarioScreen();

void halloween_countdown(int myDaysRemaining);
void thanksgiving_countdown(int myDaysRemaining);
void christmas_countdown(int myDaysRemaining);

#endif