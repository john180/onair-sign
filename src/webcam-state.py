"""

This Python script checks if my webcam is in use and posts my On-Air status to a ThingSpeak Channel.

If my webcam is in use, then I am "On-Air".

For more information: https://nothans.com/on-air-light-for-microsoft-teams-and-zoom-meetings

Dependencies

    * pip install opencv-python
    * pip install requests
    * pip install pathlib
    
Setup
    
    * Create ThingSpeak channel at https://thingspeak.com with Field 1 and Field 2 enabled
    * Copy OnAir.example.ini to OnAir.ini. Add your channel's Write API Key to the WriteKey key in the thingspeak section.
    * Set the number of cameras to the number of cameras on your system.
    * Schedule this script to run once a minute using the windows task scheduler

Created: Feb 11, 2021 by Hans Scharler (http://www.nothans.com)
Modified: Nov 30, 2021 by John Wohlers

"""
import cv2
import sys
import requests
import ctypes
import time
import subprocess
import pathlib

from configparser import ConfigParser


path = pathlib.Path(__file__).parent.resolve()
config = ConfigParser()
dataset = config.read(str(path) + '/OnAir.ini')

if len(dataset) != 1:
    raise ValueError("Failed to open config file.")

thingspeakWriteURL = 'https://api.thingspeak.com/update.json'

try:
    thingSpeakWriteKey = config.get('thingspeak', 'writekey')
    cameraCount = config.getint('cameras', 'cameracount')
except:
    print('Could not read configuration file')
    sys.exit(1)  


user32 = ctypes.windll.User32

def returnWebcamStatus(webcamIndex):

    webcam = cv2.VideoCapture(webcamIndex, cv2.CAP_DSHOW)
    if webcam.isOpened():
        webcam.release()
        return True #Webcam not in use
    else:
        return False #Webcam in use


def postToThingSpeak(valueToPost):

    thingSpeakWriteData = {'field1': valueToPost, 'api_key': thingSpeakWriteKey}

    thingspeakRequest = requests.post(thingspeakWriteURL, data = thingSpeakWriteData)

    return

def main():
    time.sleep(5)
    
    # Status flag
    onAir = 0
    errors = 0

    # Workstation lock check kludge:  https://stackoverflow.com/questions/34514644/in-python-3-how-can-i-tell-if-windows-is-locked
    process_name='LogonUI.exe'
    callall='TASKLIST'
    outputall=subprocess.check_output(callall)
    outputstringall=str(outputall)
    
    if process_name in outputstringall:
        print('Workstation Locked - Nothing to do')
        sys.exit()
    else:
        index = 0
        arr = []
        i = cameraCount
        print("Camera", "Status")
        print("------", "------")
        while i > 0:
            try:
                webcamStatus = returnWebcamStatus(index)
                print("    %1d      %1d" % (index, webcamStatus))
                if webcamStatus != True:
                    onAir = 1
                index += 1
                i -= 1
            except:
                errors += 1            
                
        # Post On-Air to Field 1 of a ThingSpeak Channel
        print("---------------")
        print("OnAir Status:", onAir)
        postToThingSpeak(onAir) 


if __name__ == '__main__':
    main()